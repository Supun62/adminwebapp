import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import { EventDTO } from 'src/app/modules/EventDTO';
import { Event_Status } from 'src/app/modules/Event_Status.enum';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  list : string[] = ['a','b','c','d','e'];

  eventDTO : EventDTO[];
  pendingEventList : EventDTO[];
  createdEventList : EventDTO[];
  publishedEventList : EventDTO[];
  targetBudgetAchievedEventList : EventDTO[];
  bannedEventList : EventDTO[];
  cancelledEventList : EventDTO[];
  heldEventList : EventDTO[];

  constructor(private eventService : EventService,private _snackBar: MatSnackBar) { 
    
    this.eventService.getPendingEvents().subscribe(x => {
        this.pendingEventList = x;
    });

    this.eventService.getCreatedEvents().subscribe(x => {
      this.createdEventList = x;
    });

    this.eventService.getTargetBudgetAchieved().subscribe(x => {
      this.targetBudgetAchievedEventList = x;
    });

    this.eventService.getPublishedEvents().subscribe(x => {
      this.publishedEventList = x;
    });

    this.eventService.getBannedEvents().subscribe(x => {
      this.bannedEventList = x;
    });

    this.eventService.getCancelledEvents().subscribe(x => {
      this.cancelledEventList = x;
    });

    this.eventService.getHeldEvents().subscribe(x => {
      this.heldEventList = x;
    });




  }

  ngOnInit() {
  }

  onBannedEvent(eventDTO : EventDTO) {
    this.eventService.bannedEvent(eventDTO.id).subscribe(x=> {
      if(x){
        this.pendingEventList = this.pendingEventList.filter(t => t.id !== eventDTO.id);
        this.openSnackBar("Ban the Event Successfull","");
      }else{
        this.openSnackBar("Ban the Event Failed","");
      }
      
    });
  }

  onApproveEvent(eventDTO : EventDTO) {
    this.eventService.approveEvent(eventDTO.id).subscribe(x=> {
      if(x){
        this.pendingEventList = this.pendingEventList.filter(t => t.id !== eventDTO.id);
        this.openSnackBar("Event Approved","");
      }else{
        this.openSnackBar("Event Approved Failed","");
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
