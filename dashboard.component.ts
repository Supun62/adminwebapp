import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { DashboardNavigation } from 'src/app/modules/DashboardNavigation.enum';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  eventsClick : boolean;
  usersClick : boolean;
  artistsClick : boolean;

  navView : String;
  
  constructor(private authenticationService : AuthenticationService,private router : Router) { 
    this.setNavigationHoldView(true,false,false);
    this.navView = DashboardNavigation[DashboardNavigation.EVENTS];

    let user = authenticationService.currentUserValue;
    
  }

  ngOnInit() {
  }

  // events
  setClassEvents() {

    let classes = {
      "is-clicked-events" : this.eventsClick
    }

    return classes;
  }

  // artists
  setClassArtists() {

    let classes = {
      "is-clicked-artists" : this.artistsClick
    }

    return classes;
  }

  // users
  setClassUsers() {

    let classes = {
      "is-clicked-users" : this.usersClick
    }

    return classes;
  }

 

  onEventsClick() {
    
    this.setNavigationHoldView(true,false,false);
    this.navView = DashboardNavigation[DashboardNavigation.EVENTS];
    
  }

  onUsersClick() {
    
    this.setNavigationHoldView(false,true,false);
    this.navView = DashboardNavigation[DashboardNavigation.USERS];
  
  }

  onArtistsClick() {
    
    this.setNavigationHoldView(false,false,true);
    this.navView = DashboardNavigation[DashboardNavigation.ARTISTS];
   
  }

  setNavigationHoldView(
    eventsClick : boolean,
    usersClick : boolean,
    artistsClick : boolean){
    this.eventsClick = eventsClick;
      this.usersClick = usersClick;
      this.artistsClick = artistsClick;
  }
}
