import { Component, OnInit, Input } from '@angular/core';
import { ArtistDTO } from 'src/app/modules/ArtistDTO';

@Component({
  selector: 'app-confirmedartists',
  templateUrl: './confirmedartists.component.html',
  styleUrls: ['./confirmedartists.component.css']
})
export class ConfirmedartistsComponent implements OnInit {

  @Input() artistDTO : ArtistDTO;

  name : string;

  constructor() { }

  ngOnInit() {
    this.name = this.artistDTO.name;
  }

}
