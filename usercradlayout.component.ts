import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-usercradlayout',
  templateUrl: './usercradlayout.component.html',
  styleUrls: ['./usercradlayout.component.css']
})
export class UsercradlayoutComponent implements OnInit {

  constructor( private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {

      this.matIconRegistry.addSvgIcon(
        "search_icon",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assests/")
      );

     }

  ngOnInit() {
  }

}
