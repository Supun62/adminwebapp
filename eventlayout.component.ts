import {ChangeDetectionStrategy, OnInit , Component, ViewEncapsulation, Input , EventEmitter ,Output } from '@angular/core';
import { EventDTO } from 'src/app/modules/EventDTO';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Event_Status } from 'src/app/modules/Event_Status.enum';
import { LocationDTO } from 'src/app/modules/LocationDTO';
import { ArtistDTO } from 'src/app/modules/ArtistDTO';
import { TicketPackageDTO } from 'src/app/modules/TicketPackageDTO';
import { EventService } from 'src/app/services/event.service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-eventlayout',
  templateUrl: './eventlayout.component.html',
  styleUrls: ['./eventlayout.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventlayoutComponent implements OnInit {

  items = Array.from({length: 100000}).map((_, i) => `Item #${i}`);

   tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 2, rows: 1, color: ''},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'}
  ];

  @Input() eventDTO : EventDTO;
  @Output() onApproveEvent : EventEmitter<EventDTO> = new EventEmitter();
  @Output() onBannedEvent : EventEmitter<EventDTO> = new EventEmitter();

  eventStatusText : string;
  fullDate : string;
  date : string;
  time : string;
  status : boolean;
  pending : boolean;

  preOrderClosingDate : string;
  eventDueDate : string;
  noOfVotes : number;

  locationDTOList : LocationDTO[];
  artistDTOList : ArtistDTO[];
  ticketPackageDTOList : TicketPackageDTO[];

  constructor(private sanitizer: DomSanitizer,private eventService : EventService) {
    this.pending = false;
   }

  ngOnInit() {
    if(this.eventDTO.eventDueDate !== null){
      this.eventStatusText = "published event on ";
      this.fullDate = this.eventDTO.publishedDateTime;
      this.status = true;
      this.eventDueDate = this.eventDTO.eventDueDate;
      
    }else{
      this.eventStatusText = "created event on ";
      this.fullDate = this.eventDTO.createdDateTime;
      this.status = false;
      this.preOrderClosingDate = this.eventDTO.preOrderClosingDate;
      
    }

    this.noOfVotes = this.eventDTO.noOfVotes;
    this.locationDTOList = this.eventDTO.locationDTOS;
    this.artistDTOList = this.eventDTO.artistDTOS;
    this.ticketPackageDTOList = this.ticketPackageDTOList;
    this.date = this.fullDate.split('T')[0];
    this.time = this.fullDate.split('T')[1].split('',5).join('');

    if(this.eventDTO.status === Event_Status.PENDING){
        this.pending = true;
    }
  }

  onBanned(){
    this.onBannedEvent.emit(this.eventDTO);
  }

  onApprove(){
    this.onApproveEvent.emit(this.eventDTO);
   
  }


}
