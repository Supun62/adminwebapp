import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  isAuthenticated : boolean;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private authenticationService : AuthenticationService, private router: Router
  ){
    this.matIconRegistry.addSvgIcon(
      "audelia_logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/logos/Logo.svg")
    ).addSvgIcon("profile_icon",this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/profile_24px.svg"))
    .addSvgIcon("view_profile_icon",this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/View-Profile-36dp.svg"))
    .addSvgIcon("settings_icon",this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/settings-36dp.svg"))
    .addSvgIcon("logout_icon",this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/logout.svg"))
    .addSvgIcon("",this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/"));

    let user = authenticationService.currentUserValue;
    if(user){
      this.isAuthenticated = true;
    }else{
      this.isAuthenticated = false;
    }
  }

  ngOnInit() {
  }



  logout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }


  profileView(){
      
  }

}
