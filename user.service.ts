import { Injectable } from '@angular/core';
import { AppConstants } from '../modules/AppConstants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../modules/UserDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userUrl : string = `${AppConstants.BACKEND_URL}${"admin"}`;
  getAllFansUrl : string = "getAllFans";
  getAllEventOrganizersUrl : string = "getAllEventOrganizers";
  getAllFacilitatorsUrl : string = "getAllFacilitators";

  constructor(private http : HttpClient) { }

  getAllFans() : Observable<UserDTO[]>{
      return this.http.get<UserDTO[]>(`${this.userUrl}/${this.getAllFansUrl}`);
  }

  getAllEventOrganizers() : Observable<UserDTO[]>{
    return this.http.get<UserDTO[]>(`${this.userUrl}/${this.getAllEventOrganizersUrl}`);
}


getAllFacilitators() : Observable<UserDTO[]>{
  return this.http.get<UserDTO[]>(`${this.userUrl}/${this.getAllFacilitatorsUrl}`);
}

}
