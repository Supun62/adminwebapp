import { EventFacilitator_Status } from './EventFacilitator_Status.enum';

export class ArtistDTO {
    name : string;
    id : number;
    artistImage : string;
    imageFile : string;
    state : EventFacilitator_Status;
    no_of_votes : number;
    artist_status : string;
}