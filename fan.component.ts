import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from 'src/app/modules/UserDTO';

@Component({
  selector: 'app-fan',
  templateUrl: './fan.component.html',
  styleUrls: ['./fan.component.css']
})
export class FanComponent implements OnInit {

  @Input() userDTO : UserDTO;

  name : string;
  email : string;
  district : string;


  constructor() { }

  ngOnInit() {
    this.name = this.userDTO.name;
    this.email = this.userDTO.email;
    this.district = this.userDTO.district;
  }

}
